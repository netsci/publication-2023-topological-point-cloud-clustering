# Main Paper

The main paper "Topological Point Cloud Clustering", Vincent P. Grande and Michael T. Schaub, accepted at ICML 2023, can be found under <https://arxiv.org/abs/2303.16716>.


# Requirements

* Python 3.9.7
* Matlab 2022b with Statistics Toolbox
* Packages in requirements.txt
* DiSC <https://users.isy.liu.se/cvl/zografos/subspace_clustering/index.html>, put at ./MatLabClustering/DiSC, requires prtools
* 7WJI.cif from <https://www.rcsb.org/structure/7WJI>

# Funding

VPG acknowledges funding by the German Research Council (DFG) within Research Training Group 2236 (UnRAVeL).
MTS acknowledges partial funding from the Ministry of Culture and Science (MKW) of the German State of North Rhine-Westphalia ("NRW Rückkehrprogramm") and the European Union (ERC, HIGH-HOPeS, 101039827). Views and opinions expressed are however those of the author(s) only and do not necessarily reflect those of the European Union or the European Research Council Executive Agency. Neither the European Union nor the granting authority can be held responsible for them.

# Personal Homepage

More information on related projects and TDA can be found under <https://vincent-grande.github.io>.